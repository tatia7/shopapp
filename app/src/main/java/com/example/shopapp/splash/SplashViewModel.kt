package com.example.shopapp.splash

import androidx.lifecycle.ViewModel
import com.example.shopapp.userData.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class SplashViewModel @Inject constructor(private val user: User) : ViewModel() {

    fun isLogged() = user.hasSession()

}