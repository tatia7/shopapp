package com.example.shopapp.splash


import android.animation.Animator
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.databinding.FragmentSplashBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding,SplashViewModel>(FragmentSplashBinding::inflate,SplashViewModel::class.java) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        binding!!.animView.playAnimation()
        binding!!.animView.addAnimatorListener(object : Animator.AnimatorListener{
            override fun onAnimationStart(animation: Animator?) {
            }
            override fun onAnimationEnd(animation: Animator?) {
                navigate()
            }
            override fun onAnimationCancel(animation: Animator?) {
            }
            override fun onAnimationRepeat(animation: Animator?) {
            }
        })
    }
    private fun navigate(){
        if(viewModel.isLogged()){
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        }else{
            findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
        }
    }
}