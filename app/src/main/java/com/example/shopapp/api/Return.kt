package com.example.shopapp.api

data class Return<T>(val status : Status, val data : T? = null, val message : String? = null, val loading : Boolean){

    enum class Status {
        Success,
        Error,
        Loading
    }
    companion object{

        fun<T> success(data : T): Return<T>{
            return Return(Status.Success,data,null,false)
        }

        fun<T> error(message: String?) : Return<T>{
            return Return(Status.Error, null,message, false)
        }

        fun<T> loading(loading: Boolean) : Return<T>{
            return Return(Status.Loading,null,null,loading)
        }
    }
}