package com.example.shopapp.container

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.databinding.FragmentContainerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ContainerFragment : BaseFragment<FragmentContainerBinding, ContainerViewModel>(FragmentContainerBinding::inflate,ContainerViewModel::class.java) {


    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        initBottomNav()
    }

    private fun initBottomNav(){
        val navController = childFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        binding!!.bottomNav.setupWithNavController(navController.navController)
    }


}