package com.example.shopapp.module

import android.net.Uri
import androidx.annotation.RestrictTo
import androidx.viewbinding.BuildConfig
import com.example.shopapp.api.ApiService
import com.example.shopapp.repository.LoginRep
import com.example.shopapp.repository.LoginRepImplemen
import com.example.shopapp.userData.User
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.internal.http2.Http2Connection
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    companion object{
        const val BASE_URL = "https://ktorhighsteaks.herokuapp.com/"
    }
    private fun httpClient() : OkHttpClient{
        val builder = OkHttpClient.Builder()
            .addInterceptor(Interceptor { chain ->
                chain.request().url()
                val request = chain.request().newBuilder()

                val response = chain.proceed(request.build())
                when(response.code()){
                    400,500,403 -> {}
                    401 -> {}
                }
                response
            })
        ///if (BuildConfig.BUILD_TYPE == "debug"){
         //   builder.addInterceptor
        //}
        return builder.build()
    }
    @Provides
    @Singleton
    fun login() = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideLogRep(apiservice: ApiService, user : User) : LoginRep = LoginRepImplemen(apiservice, user )
}