package com.example.shopapp.extencions

import android.view.View

fun View.hide(){
    visibility = View.INVISIBLE
}
fun View.show(){
    visibility = View.VISIBLE
}
fun View.gone(){
    visibility = View.GONE
}
fun View.hideIf(isHide: Boolean){
    if (isHide){
        hide()
    }else{
        show()
    }
}