package com.example.shopapp.userData

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class User @Inject constructor(@ApplicationContext context: Context) {

    companion object {
        const val SESSION = "HAS_SESSION"
        const val TOKEN = "TOKEN"
    }

    private val sharedPreference: SharedPreferences by lazy {
        context.getSharedPreferences("user", Context.MODE_PRIVATE)
    }


    fun saveSession(session: Boolean) {
        sharedPreference.edit().putBoolean(SESSION, session).apply()
    }
    fun hasSession() = sharedPreference.getBoolean(SESSION, false)
    fun saveToken(token : String){
        sharedPreference.edit().putString(TOKEN, token).apply()
    }
    fun token() = sharedPreference.getBoolean(TOKEN, "".toBoolean())

}