package com.example.shopapp.repository

import com.example.shopapp.api.Return
import com.example.shopapp.model.Login
import com.example.shopapp.model.Register

interface LoginRep {

    suspend fun login(email : String, password : String, remember : Boolean): Return<Login>

    suspend fun signup(email : String, fullname : String, password : String, repeatPass : String): Return<Register>
}