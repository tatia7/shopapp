package com.example.shopapp.repository

import com.example.shopapp.api.ApiService
import com.example.shopapp.api.Return
import com.example.shopapp.model.Login
import com.example.shopapp.model.Register
import com.example.shopapp.userData.User
import com.google.gson.Gson
import java.lang.Exception
import javax.inject.Inject

class LoginRepImplemen @Inject constructor(val apiService: ApiService, private val user : User) : LoginRep{
    override suspend fun login(email: String, password: String, remember : Boolean): Return<Login> {
        return try {
            val response = apiService.login(email, password)
            if (response.isSuccessful){
                val body = response.body()!!
                user.saveSession(remember)
                user.saveToken(body.token)
                Return.success(body)
            }
            else {
                val error = response.errorBody()!!.string()
                val errorModel = Gson().fromJson(error, Error::class.java)
                Return.error(error)
            }
        } catch (e: Exception){
            return Return.error(e.message.toString())
        }
    }

    override suspend fun signup(
        email: String,
        fullname: String,
        password: String,
        repeatPass: String
    ): Return<Register> {
        return try {
            val response = apiService.register(email,fullname, password)
            if (response.isSuccessful) {

                val body = response.body()!!

                Return.succsess(body)
            } else {
                //val errorModel = Gson().fromJson(response.errorBody()!!.string(),Error::class.java)
                Return.error(response.errorBody()!!.string())
            }
        } catch (e: Exception) {
            Return.error(e.message.toString())
        }
    }
}