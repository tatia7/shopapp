package com.example.shopapp.login

import android.app.Dialog
import android.util.Patterns
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.api.Return
import com.example.shopapp.databinding.LoginFragmentBinding
import com.example.shopapp.extencions.isEmail
import com.example.shopapp.extencions.setUp

class LoginFragment : BaseFragment<LoginFragmentBinding, LoginViewModel>(
    LoginFragmentBinding::inflate,
    LoginViewModel::class.java
) {
    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        btnClick()
        observes()
    }
    private fun btnClick(){
        binding?.logInBtn?.setOnClickListener {login()}
    }
    private fun observes(){
        viewModel._loginLiveData.observe(viewLifecycleOwner, {

            when (it.status) {

                Return.Status.Success -> {
                    viewModel.session(binding!!.rememberMe.isChecked)
                }
                Return.Status.Error -> {
                    it.message?.let { it1 -> showErrorDialog("Error", it1) }
                }

            }

        })
    }
    private fun login(){

        val email = binding!!.email.text.toString().trim()
        val password = binding!!.password.text.toString().trim()

        if (email.isNotBlank() && password.isNotBlank()) {
            if (email.isEmail()) {
                viewModel.login(email, password, binding!!.rememberMe.isChecked)
            }
            else{
                showErrorDialog("Email Not Valid", "Enter Valid Email")
            }
        } else {
            showErrorDialog("Error", "Fill all fields")
        }

    }
    fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.show()
    }

}