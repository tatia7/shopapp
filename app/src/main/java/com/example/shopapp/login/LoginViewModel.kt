package com.example.shopapp.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shopapp.api.Return
import com.example.shopapp.model.Login
import com.example.shopapp.repository.LoginRep
import com.example.shopapp.userData.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(val loginRep: LoginRep, val user : User) : ViewModel(){

    private var loginLiveData = MutableLiveData<Return<Login>>()

    var _loginLiveData : LiveData<Return<Login>> = loginLiveData
    fun login(email: String, password : String, remember : Boolean){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val result = loginRep.login(email,password, remember)
                loginLiveData.postValue(result)
            }
        }
    }
    fun session(isChecked : Boolean){
        user.saveSession(isChecked)
    }
}