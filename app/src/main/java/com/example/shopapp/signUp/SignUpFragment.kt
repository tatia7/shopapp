package com.example.shopapp.signUp

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.api.Return
import com.example.shopapp.api.Return.Companion.error
import com.example.shopapp.databinding.FragmentSignUpBinding
import com.example.shopapp.extencions.isEmail
import com.example.shopapp.extencions.setUp

class SignUpFragment : BaseFragment<FragmentSignUpBinding, SignUpViewModel>(FragmentSignUpBinding::inflate, SignUpViewModel::class.java){

    override val viewModel : SignUpViewModel by viewModels()
    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {

        listeners()
        observes()

    }
    private fun listeners() {

        binding!!.signUpBtn.setOnClickListener {
            signUp()

        }
        binding!!.logInBtn.setOnClickListener {
            findNavController().navigateUp()
        }
    }
    private fun signUp() {

        val email = binding!!.email.text.toString().trim()
        val password = binding!!.password.text.toString().trim()
        val fullname = binding!!.fullName.text.toString().trim()
        val repeatPassword = binding!!.repeatPassword.text.toString().trim()


        if (email.isNotBlank() && password.isNotBlank() && fullname.isNotBlank()) {


            if (email.isEmail()) {
                if (password == repeatPassword) {
                    viewModel.signup(email, fullname, password, "")
                } else {
                    showErrorDialog(getString("Error".toInt()), "password Desnt Match")
                }
            } else {
                showErrorDialog(getString("Error".toInt()), "email is incorrect")
            }


        } else {
            showErrorDialog(getString("Error".toInt()), getString("Missing Fields!".toInt()))
        }
    }
    private fun observes() {
        viewModel._registerLiveData.observe(viewLifecycleOwner, {

            when (it.status) {

                Return.Status.Success -> {
                    findNavController().navigate(R.id.action_signUpFragment_to_homeFragment)
                }
                Return.Status.Error -> {
                    showErrorDialog(getString("Error".toInt()), it.message!!)
                }
            }
        })
    }


    fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.conti).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }

}