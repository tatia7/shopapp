package com.example.shopapp.signUp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shopapp.api.Return
import com.example.shopapp.model.Login
import com.example.shopapp.model.Register
import com.example.shopapp.repository.LoginRep
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
        private val loginRep: LoginRep
) : ViewModel(){

    private var registerLiveData = MutableLiveData<Return<Register>>()

    var _registerLiveData : LiveData<Return<Register>> = registerLiveData
    fun signup(email: String, fullname : String, password : String, repeatPass: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val result = loginRep.signup(email,fullname,password,repeatPass)
                registerLiveData.postValue(result)
            }
        }
    }
}