package com.example.shopapp.home

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.databinding.FragmentHomeBinding

class HomeFragment : BaseFragment<FragmentHomeBinding,HomeViewModel>(FragmentHomeBinding::inflate,HomeViewModel::class.java) {


    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

    }


}