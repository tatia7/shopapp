package com.example.shopapp.model

import com.google.gson.annotations.SerializedName

data class Register(
    @SerializedName("OK")
    val ok : Boolean,
    @SerializedName("registered")
    val registered : Boolean
)
