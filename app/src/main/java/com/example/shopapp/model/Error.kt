package com.example.shopapp.model

data class Error(val ok : Boolean, val error: String)
